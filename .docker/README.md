# Docker instructions

## For production

### Step 1: Create an isolated network for your application containers
```shell
docker network create flaskr
```

### Step 2: Setup and launch the database

```shell
docker pull postgres:11-alpine
docker run --name flaskr-db --network=flaskr -p 5432:5432 -e POSTGRES_USER=flaskr -e POSTGRES_PASSWORD=flaskr -d postgres:11-alpine

```

### Step 3: Build application image
```shell
docker build -t <username>/flaskr .
```

### Step 4 (optional): Initialize database if necessary

```shell
docker run --rm --network=flaskr -e DATABASE_URL="postgres://flaskr:flaskr@flaskr-db:5432/flaskr" <username>/flaskr flask init-db
```

### Step 5: Launch application
```shell
docker run --name flaskr-app --network=flaskr -p 8080:5000 -e DATABASE_URL="postgres://flaskr:flaskr@flaskr-db:5432/flaskr" -e SECRET_KEY="<random_key>" -d <username>/flaskr
```

### Step 6 (optional): See logs of your application
```shell
docker logs -f flaskr-app
```

## For development

### RECOMMENDED: Before running docker-compose

Copy `.env.dist` to `.env` and set appropriate value for `FLASKR_SECRET_KEY` env variable.

You may generate a random secret using this command:

```shell
openssl rand -base64 32
```

### Start in foreground

```shell
docker-compose up -d
```

### Execute in background

```shell
docker-compose up -d
```

### Stop and remove containers

```
docker-compose down
```
