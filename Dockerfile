# We want to use Python as base image
# The database will be separated from this image
FROM python:3-alpine

# Thomas Domingues is the author of this Dockerfile
LABEL maintainer="thomas.domingues@protonmail.com"

# Default environment variables
ENV DATABASE_URL="postgres://flaskr:flaskr@localhost:5432/flaskr"
ENV SECRET_KEY="this_secret_is_not_secure_please_override_it"

# Set default Flask settings
ENV FLASK_APP="flaskr"
ENV FLASK_ENV="production"

# Copy application code in '/app' folder
COPY . /app

# Use '/app' as default working directory
WORKDIR /app

# Install specific system packages for some Python dependencies (here, psycopg2)
RUN apk add --no-cache \
    postgresql-dev=11.6-r0 \
    gcc=8.3.0-r0 \
    python3-dev=3.6.9-r2 \
    musl-dev=1.1.20-r5

# Install app dependencies
RUN pip install -r requirements.txt

# By default, run
CMD [ "flask", "run", "--host=0.0.0.0"]
